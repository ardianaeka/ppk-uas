package com.example.uassupermart.ListAdapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.uassupermart.Model.Pegawai;
import com.example.uassupermart.R;
import com.example.uassupermart.RecyclerViewInterface;

import java.util.ArrayList;

public class PegawaiListAdapter extends RecyclerView.Adapter<PegawaiListAdapter.ViewHolder> {
    private final RecyclerViewInterface recyclerViewInterface;
    private ArrayList<Pegawai> listPegawai;

    public PegawaiListAdapter(ArrayList<Pegawai> listPegawai, RecyclerViewInterface recyclerViewInterface) {
        this.listPegawai = listPegawai;
        this.recyclerViewInterface = recyclerViewInterface;
    }

    @Override
    public PegawaiListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.row_pegawai, parent, false);
        return new ViewHolder(view, recyclerViewInterface);
    }

    @Override
    public void onBindViewHolder(@NonNull PegawaiListAdapter.ViewHolder holder, int position) {
        holder.id.setText(String.valueOf(listPegawai.get(position).getId()));
        holder.nomorpegawai.setText(listPegawai.get(position).getNomorpegawai());
        holder.nama.setText(listPegawai.get(position).getNama());
        holder.email.setText(listPegawai.get(position).getEmail());
        holder.username.setText(listPegawai.get(position).getUsername());
        holder.notelp.setText(listPegawai.get(position).getNotelp());
    }

    @Override
    public int getItemCount()  {
        return (listPegawai != null) ? listPegawai.size() : 0;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView id, nomorpegawai, nama, email, username, notelp;

        public ViewHolder(@NonNull View itemView, RecyclerViewInterface recyclerViewInterface) {
            super(itemView);
            id = itemView.findViewById(R.id.id_pegawai);
            nomorpegawai = itemView.findViewById(R.id.nomorpegawai);
            nama = itemView.findViewById(R.id.nama);
            email = itemView.findViewById(R.id.email);
            username = itemView.findViewById(R.id.username);
            notelp = itemView.findViewById(R.id.notelp);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (recyclerViewInterface != null) {
                        int pos = getAdapterPosition();
                        if (pos != RecyclerView.NO_POSITION) {
                            recyclerViewInterface.onItemClick(pos);
                        }
                    }
                }
            });
        }
    }
}
