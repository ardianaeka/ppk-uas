package com.example.uassupermart.ListAdapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.uassupermart.Model.Produk;
import com.example.uassupermart.R;
import com.example.uassupermart.RecyclerViewInterface;

import java.util.ArrayList;

public class ProdukListAdapter extends RecyclerView.Adapter<ProdukListAdapter.ViewHolder> {
    private final RecyclerViewInterface recyclerViewInterface;
    private ArrayList<Produk> listProduk;

    public ProdukListAdapter(ArrayList<Produk> listProduk, RecyclerViewInterface recyclerViewInterface) {
        this.listProduk = listProduk;
        this.recyclerViewInterface = recyclerViewInterface;
    }

    public void setFilteredProduct(ArrayList<Produk> filteredProduct) {
        this.listProduk = filteredProduct;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.row_barang, parent, false);
        return new ViewHolder(view, recyclerViewInterface);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.id.setText(String.valueOf(listProduk.get(position).getId()));
        holder.nama.setText(listProduk.get(position).getNama());
        holder.harga.setText(listProduk.get(position).getHarga());
        holder.kategori.setText(listProduk.get(position).getKategori());
        holder.stok.setText(String.valueOf(listProduk.get(position).getStok()));
    }

    @Override
    public int getItemCount() {
        return (listProduk != null) ? listProduk.size() : 0;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView id, nama, harga, kategori, stok;

        public ViewHolder(@NonNull View itemView, RecyclerViewInterface recyclerViewInterface) {
            super(itemView);
            id = itemView.findViewById(R.id.id_barang);
            nama = itemView.findViewById(R.id.nama_barang);
            harga = itemView.findViewById(R.id.harga_satuan);
            kategori = itemView.findViewById(R.id.kategori_barang);
            stok = itemView.findViewById(R.id.stok_barang);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (recyclerViewInterface != null) {
                        int pos = getAdapterPosition();
                        if (pos != RecyclerView.NO_POSITION) {
                            recyclerViewInterface.onItemClick(pos);
                        }
                    }
                }
            });
        }
    }
}
