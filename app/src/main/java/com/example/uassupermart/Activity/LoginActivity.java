package com.example.uassupermart.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.uassupermart.Model.JWTToken;
import com.example.uassupermart.Model.User;
import com.example.uassupermart.R;
import com.example.uassupermart.Service.APICall;
import com.example.uassupermart.Service.Retro;
import com.example.uassupermart.Service.TokenManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    EditText editTextEmail, editTextPassword;
    TextView textViewRegister;
    Button loginButton;
    ImageView backButton;

    final APICall apiCall = Retro.getAPICall();
    private TokenManager tokenManager;
    private static String token;
    private static String id;
    private static String email;
    private static String role;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initView();
        initOnClickListener();
    }

    private void initView() {
        editTextEmail = findViewById(R.id.email);
        editTextPassword = findViewById(R.id.password);
        loginButton = findViewById(R.id.loginButton);
        backButton = findViewById(R.id.backImage);
        textViewRegister = findViewById(R.id.signupLinkText);
    }

    private void initOnClickListener() {
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, StartActivity.class));
                finish();
            }
        });

        editTextEmail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                // when losses focus
                if (!hasFocus) {
                    editTextEmail.setBackground(AppCompatResources.getDrawable(LoginActivity.this, R.drawable.custom_edit_text_nofocus));
                    editTextEmail.setCompoundDrawablesWithIntrinsicBounds(null, null, AppCompatResources.getDrawable(LoginActivity.this, R.drawable.fi_rr_user_nofocus), null);
                } else {
                    editTextEmail.setBackground(AppCompatResources.getDrawable(LoginActivity.this, R.drawable.custom_edit_text));
                    editTextEmail.setCompoundDrawablesWithIntrinsicBounds(null, null, AppCompatResources.getDrawable(LoginActivity.this, R.drawable.fi_rr_user), null);
                }
            }
        });

        editTextPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                // when losses focus
                if (!hasFocus) {
                    editTextPassword.setBackground(AppCompatResources.getDrawable(LoginActivity.this, R.drawable.custom_edit_text_nofocus));
                    editTextPassword.setCompoundDrawablesWithIntrinsicBounds(null, null, AppCompatResources.getDrawable(LoginActivity.this, R.drawable.fi_rr_unlock_nofocus), null);
                } else {
                    editTextPassword.setBackground(AppCompatResources.getDrawable(LoginActivity.this, R.drawable.custom_edit_text));
                    editTextPassword.setCompoundDrawablesWithIntrinsicBounds(null, null, AppCompatResources.getDrawable(LoginActivity.this, R.drawable.fi_rr_unlock), null);
                }
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(editTextEmail.getText().toString().equals("")) {
                    Toast.makeText(LoginActivity.this, "Mohon masukkan email", Toast.LENGTH_SHORT).show();
                    return;
                } else if(editTextPassword.getText().toString().equals("")) {
                    Toast.makeText(LoginActivity.this, "Mohon masukkan password", Toast.LENGTH_SHORT).show();
                    return;
                }
                login();
            }
        });

        // link to register page
        textViewRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
                finish();
            }
        });
    }

    private void login() {
        Call<JWTToken> call = apiCall.login(editTextEmail.getText().toString(), editTextPassword.getText().toString());

        call.enqueue(new Callback<JWTToken>() {
            @Override
            public void onResponse(Call<JWTToken> call, Response<JWTToken> response) {
                if (response.isSuccessful()) {
                    token = response.body().getToken();
                    Toast.makeText(LoginActivity.this, "Berhasil Login", Toast.LENGTH_SHORT).show();
                    getUser();
                } else {
                    Toast.makeText(LoginActivity.this, "Username dan password tidak sesuai", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<JWTToken> call, Throwable t) {
                Toast.makeText(LoginActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getUser() {
        Call<User> call = apiCall.getUser("Bearer " + token);

        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    id = response.body().getId();
                    email = response.body().getEmail();
                    role = response.body().getRole();
                    tokenManager = new TokenManager(getApplicationContext());
                    tokenManager.createSession(id, email, role, token, true);
                    startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                    finish();
                } else {
                    Toast.makeText(LoginActivity.this, "Token invalid", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Toast.makeText(LoginActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}