package com.example.uassupermart.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.uassupermart.R;
import com.example.uassupermart.Service.APICall;
import com.example.uassupermart.Service.Retro;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TambahProdukActivity extends AppCompatActivity {
    ImageView backBtn;
    EditText editTextNama, editTextHarga, editTextStok;
    AutoCompleteTextView autoCompleteTextViewKategori;
    Button submitButton;
    ArrayAdapter<String> adapterItems;
    String[] listKategori = {"Food", "Beverage", "Grocery", "Fresh & Vegetables", "Dairy", "Personal Care"};

    String nama_produk, harga_satuan, stok, kategori;
    SharedPreferences sharedPreferences;
    final APICall apiCall = Retro.getAPICall();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_produk);

        sharedPreferences = getSharedPreferences("JWTTOKEN", 0);

        initView();
        initOnClickListener();
    }

    private void initView() {
        backBtn = findViewById(R.id.backBtn);
        editTextNama = findViewById(R.id.editTextNama);
        editTextHarga = findViewById(R.id.editTextHarga);
        editTextStok = findViewById(R.id.editTextStok);
        autoCompleteTextViewKategori = findViewById(R.id.autoCompleteTextKategori);
        submitButton = findViewById(R.id.submitButton);

        editTextNama.setOnFocusChangeListener(onEditTextFocusChangeListener);
        editTextHarga.setOnFocusChangeListener(onEditTextFocusChangeListener);
        editTextStok.setOnFocusChangeListener(onEditTextFocusChangeListener);
        adapterItems = new ArrayAdapter<String>(this, R.layout.list_kategori, listKategori);
        autoCompleteTextViewKategori.setAdapter(adapterItems);
    }

    private void initOnClickListener() {
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        autoCompleteTextViewKategori.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                kategori = adapterView.getItemAtPosition(i).toString();
            }
        });

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // validasi input
                if(editTextNama.getText().toString().equals("")) {
                    Toast.makeText(TambahProdukActivity.this, "Mohon masukkan nama produk", Toast.LENGTH_SHORT).show();
                    return;
                } else if(editTextHarga.getText().toString().equals("")) {
                    Toast.makeText(TambahProdukActivity.this, "Mohon masukkan harga satuan", Toast.LENGTH_SHORT).show();
                    return;
                } else if(editTextStok.getText().toString().equals("")) {
                    Toast.makeText(TambahProdukActivity.this, "Mohon masukkan stok produk", Toast.LENGTH_SHORT).show();
                    return;
                } else if(autoCompleteTextViewKategori.getAdapter().isEmpty()) {
                    Toast.makeText(TambahProdukActivity.this, "Mohon pilih kategori produk", Toast.LENGTH_SHORT).show();
                    return;
                }

                nama_produk = editTextNama.getText().toString();
                harga_satuan = editTextHarga.getText().toString();
                stok = editTextStok.getText().toString();
                createProduk();
            }
        });
    }

    private View.OnFocusChangeListener onEditTextFocusChangeListener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (hasFocus) {
                ((EditText)v).setHintTextColor(getResources().getColor(R.color.gray_d));
            } else {
                ((EditText)v).setHintTextColor(getResources().getColor(R.color.white));
            }
        }
    };

    private void createProduk() {
        String token = sharedPreferences.getString("token", "token");

        if(!token.equals("token")) {
            Call<ResponseBody> createProduct = apiCall.createProduct("Bearer " + token, nama_produk, harga_satuan, stok, kategori);

            createProduct.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful()) {
                        Toast.makeText(TambahProdukActivity.this, "Berhasil menambahkan barang", Toast.LENGTH_SHORT).show();
                        finish();
                        startActivity(new Intent(TambahProdukActivity.this, InventarisActivity.class));
                    } else {
                        Toast.makeText(TambahProdukActivity.this, "Gagal menambahkan barang", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Toast.makeText(TambahProdukActivity.this, "onFailure " + t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(TambahProdukActivity.this, "Token Required", Toast.LENGTH_SHORT).show();
        }
    }
}