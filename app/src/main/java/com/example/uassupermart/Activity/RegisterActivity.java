package com.example.uassupermart.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.uassupermart.Model.JWTToken;
import com.example.uassupermart.Model.User;
import com.example.uassupermart.R;
import com.example.uassupermart.Service.APICall;
import com.example.uassupermart.Service.Retro;
import com.example.uassupermart.Service.TokenManager;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {

    EditText namaLengkap, username, editTextEmail, password, confirmPassword;
    TextView login;
    Button signupButton;
    ImageView backButton;

    final APICall apiCall = Retro.getAPICall();

    private TokenManager tokenManager;
    private static String token;
    private static String id;
    private static String email;
    private static String role;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        initView();
        initOnClickListener();
    }

    private void initView() {
        namaLengkap = findViewById(R.id.nama_lengkap);
        username = findViewById(R.id.username);
        editTextEmail = findViewById(R.id.email);
        password = findViewById(R.id.password);
        confirmPassword = findViewById(R.id.confirm_password);
        signupButton = findViewById(R.id.signupButton);
        backButton = findViewById(R.id.backImage);
        login = findViewById(R.id.signinLinkText);

        namaLengkap.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                // when losses focus
                if (!hasFocus) {
                    namaLengkap.setBackground(AppCompatResources.getDrawable(RegisterActivity.this, R.drawable.custom_edit_text_nofocus));
                    namaLengkap.setCompoundDrawablesWithIntrinsicBounds(null, null, AppCompatResources.getDrawable(RegisterActivity.this, R.drawable.fi_rr_user_nofocus), null);
                } else {
                    namaLengkap.setBackground(AppCompatResources.getDrawable(RegisterActivity.this, R.drawable.custom_edit_text));
                    namaLengkap.setCompoundDrawablesWithIntrinsicBounds(null, null, AppCompatResources.getDrawable(RegisterActivity.this, R.drawable.fi_rr_user), null);
                }
            }
        });

        username.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                // when losses focus
                if (!hasFocus) {
                    username.setBackground(AppCompatResources.getDrawable(RegisterActivity.this, R.drawable.custom_edit_text_nofocus));
                    username.setCompoundDrawablesWithIntrinsicBounds(null, null, AppCompatResources.getDrawable(RegisterActivity.this, R.drawable.fi_rr_at_nofocus), null);
                } else {
                    username.setBackground(AppCompatResources.getDrawable(RegisterActivity.this, R.drawable.custom_edit_text));
                    username.setCompoundDrawablesWithIntrinsicBounds(null, null, AppCompatResources.getDrawable(RegisterActivity.this, R.drawable.fi_rr_at), null);
                }
            }
        });

        editTextEmail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                // when losses focus
                if (!hasFocus) {
                    editTextEmail.setBackground(AppCompatResources.getDrawable(RegisterActivity.this, R.drawable.custom_edit_text_nofocus));
                    editTextEmail.setCompoundDrawablesWithIntrinsicBounds(null, null, AppCompatResources.getDrawable(RegisterActivity.this, R.drawable.fi_rr_envelope_nofocus), null);
                } else {
                    editTextEmail.setBackground(AppCompatResources.getDrawable(RegisterActivity.this, R.drawable.custom_edit_text));
                    editTextEmail.setCompoundDrawablesWithIntrinsicBounds(null, null, AppCompatResources.getDrawable(RegisterActivity.this, R.drawable.fi_rr_envelope), null);
                }
            }
        });

        password.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                // when losses focus
                if (!hasFocus) {
                    password.setBackground(AppCompatResources.getDrawable(RegisterActivity.this, R.drawable.custom_edit_text_nofocus));
                    password.setCompoundDrawablesWithIntrinsicBounds(null, null, AppCompatResources.getDrawable(RegisterActivity.this, R.drawable.fi_rr_unlock_nofocus), null);
                } else {
                    password.setBackground(AppCompatResources.getDrawable(RegisterActivity.this, R.drawable.custom_edit_text));
                    password.setCompoundDrawablesWithIntrinsicBounds(null, null, AppCompatResources.getDrawable(RegisterActivity.this, R.drawable.fi_rr_unlock), null);
                }
            }
        });

        confirmPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                // when losses focus
                if (!hasFocus) {
                    confirmPassword.setBackground(AppCompatResources.getDrawable(RegisterActivity.this, R.drawable.custom_edit_text_nofocus));
                    confirmPassword.setCompoundDrawablesWithIntrinsicBounds(null, null, AppCompatResources.getDrawable(RegisterActivity.this, R.drawable.fi_rr_unlock_nofocus), null);
                } else {
                    confirmPassword.setBackground(AppCompatResources.getDrawable(RegisterActivity.this, R.drawable.custom_edit_text));
                    confirmPassword.setCompoundDrawablesWithIntrinsicBounds(null, null, AppCompatResources.getDrawable(RegisterActivity.this, R.drawable.fi_rr_unlock), null);
                }
            }
        });
    }

    private void initOnClickListener() {
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(RegisterActivity.this, StartActivity.class));
                finish();
            }
        });

        signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(namaLengkap.getText().toString().equals("")) {
                    Toast.makeText(RegisterActivity.this, "Mohon masukkan nama lengkap", Toast.LENGTH_SHORT).show();
                    return;
                } else if(username.getText().toString().equals("")) {
                    Toast.makeText(RegisterActivity.this, "Mohon masukkan username", Toast.LENGTH_SHORT).show();
                    return;
                } else if(editTextEmail.getText().toString().equals("")) {
                    Toast.makeText(RegisterActivity.this, "Mohon masukkan email", Toast.LENGTH_SHORT).show();
                    return;
                } else if(password.getText().toString().equals("")) {
                    Toast.makeText(RegisterActivity.this, "Mohon masukkan password", Toast.LENGTH_SHORT).show();
                    return;
                } else if(confirmPassword.getText().toString().equals("")) {
                    Toast.makeText(RegisterActivity.this, "Mohon masukkan konfirmasi password", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (password.getText().toString().equals(confirmPassword.getText().toString())) {
                    register();
                } else {
                    Toast.makeText(RegisterActivity.this, "Konfirmasi password tidak sesuai\nGagal Mendaftar", Toast.LENGTH_SHORT).show();
                }
            }
        });

        // link to login page
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                finish();
            }
        });
    }

    private void register() {
        // create user
        Call<JWTToken> call = apiCall.register(
                namaLengkap.getText().toString(),
                username.getText().toString(),
                editTextEmail.getText().toString(),
                password.getText().toString(),
                confirmPassword.getText().toString(),
                "user");

        call.enqueue(new Callback<JWTToken>() {
            @Override
            public void onResponse(Call<JWTToken> call, Response<JWTToken> response) {
                if (response.isSuccessful()) {
                    token = response.body().getToken();

                    // create pegawai
                    Call<ResponseBody> pegawai = apiCall.createPegawai("Bearer " + token,
                            "",
                            editTextEmail.getText().toString(),
                            password.getText().toString(),
                            username.getText().toString(),
                            namaLengkap.getText().toString(),
                            "",
                            "",
                            "");

                    pegawai.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            if (response.code() == 201) {
                                getUser();
                                Toast.makeText(RegisterActivity.this, "Berhasil mendaftar", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(RegisterActivity.this, "Gagal mendaftar 2", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            Toast.makeText(RegisterActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });

                } else {
                    Toast.makeText(RegisterActivity.this, "Gagal mendaftar", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<JWTToken> call, Throwable t) {
                Toast.makeText(RegisterActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getUser() {
        Call<User> call = apiCall.getUser("Bearer " + token);

        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    id = response.body().getId();
                    email = response.body().getEmail();
                    role = response.body().getRole();
                    tokenManager = new TokenManager(getApplicationContext());
                    tokenManager.createSession(id, email, role, token, true);
                    startActivity(new Intent(RegisterActivity.this, HomeActivity.class));
                    finish();
                } else {
                    Toast.makeText(RegisterActivity.this, "Token invalid", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Toast.makeText(RegisterActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}