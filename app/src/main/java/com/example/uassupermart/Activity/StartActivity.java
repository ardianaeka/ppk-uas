package com.example.uassupermart.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import androidx.appcompat.widget.AppCompatButton;

import com.example.uassupermart.R;

public class StartActivity extends AppCompatActivity {
    AppCompatButton button;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        sharedPreferences = getSharedPreferences("JWTTOKEN", 0);

        // jika session masih ada
        if (!sharedPreferences.getString("token", "token").equals("token")) {
            finish();
            startActivity(new Intent(StartActivity.this, HomeActivity.class));
        }

        button = findViewById(R.id.startButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // opening a new activity via an intent.
                Intent i = new Intent(StartActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
            }
        });
    }
}