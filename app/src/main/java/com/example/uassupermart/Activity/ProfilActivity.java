package com.example.uassupermart.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.uassupermart.Model.Pegawai;
import com.example.uassupermart.R;
import com.example.uassupermart.Service.APICall;
import com.example.uassupermart.Service.Retro;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfilActivity extends AppCompatActivity {

    TextView textViewNama, textViewUsername;
    ImageView profilPict;
    TextInputLayout textInputLayoutNomorPegawai, textInputLayoutNama, textInputLayoutEmail, textInputLayoutPassword, textInputLayoutNotelp, textInputLayoutJenisKelamin, textInputLayoutAlamat;
    EditText editTextNomorPegawai, editTextNama, editTextEmail, editTextPassword, editTextNotelp, editTextAlamat;
    Button editProfilBtn;
    LinearLayout homeBtn;
    FloatingActionButton addBtn;
    AutoCompleteTextView autoCompleteTextViewJenisKelamin;
    ArrayAdapter<String> adapterItems;
    String jenisKelamin = "";
    String[] listJenisKelamin = {"Laki-laki", "Perempuan"};

    SharedPreferences sharedPreferences;
    final APICall apiCall = Retro.getAPICall();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil);

        sharedPreferences = getSharedPreferences("JWTTOKEN", 0);
        initView();
        initOnClickListener();
        getProfilPegawai();
    }

    private void initView() {
        profilPict = findViewById(R.id.profil_pict);
        autoCompleteTextViewJenisKelamin = findViewById(R.id.autoCompleteTextJenisKelamin);
        // TextView
        textViewNama = findViewById(R.id.nama);
        textViewUsername = findViewById(R.id.username);
        // Text Input Layout
        textInputLayoutNomorPegawai = findViewById(R.id.textInputLayoutNomorPegawai);
        textInputLayoutNama = findViewById(R.id.textInputLayoutNama);
        textInputLayoutEmail = findViewById(R.id.textInputLayoutEmail);
        textInputLayoutPassword = findViewById(R.id.textInputLayoutPassword);
        textInputLayoutNotelp = findViewById(R.id.textInputLayoutNotelp);
        textInputLayoutJenisKelamin = findViewById(R.id.textInputLayoutJenisKelamin);
        textInputLayoutAlamat = findViewById(R.id.textInputLayoutAlamat);
        // Edit Text
        editTextNomorPegawai = findViewById(R.id.editTextNomorPegawai);
        editTextNama = findViewById(R.id.editTextNama);
        editTextEmail = findViewById(R.id.editTextEmail);
        editTextPassword = findViewById(R.id.editTextPassword);
        editTextNotelp = findViewById(R.id.editTextNotelp);
        editTextAlamat = findViewById(R.id.editTextAlamat);
        // Edit Profil Button
        editProfilBtn = findViewById(R.id.editProfilBtn);
        // Bottom Bar
        homeBtn = findViewById(R.id.homeBtn);
        addBtn = findViewById(R.id.addBtn);

        textInputLayoutNama.setVisibility(View.GONE);
        textInputLayoutPassword.setVisibility(View.GONE);

        adapterItems = new ArrayAdapter<String>(this, R.layout.list_kategori, listJenisKelamin);
        autoCompleteTextViewJenisKelamin.setAdapter(adapterItems);

        // jika role pengguna adalah bukan admin
        if (!sharedPreferences.getString("role", "user").equals("admin")) {
            // menyembunyikan button add barang
            addBtn.setVisibility(View.GONE);
        } else {
            addBtn.setVisibility(View.VISIBLE);
        }
    }

    private void initOnClickListener() {
        editProfilBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(editProfilBtn.getText().toString().equals("Edit Profil")) {
                    textInputLayoutNama.setVisibility(View.VISIBLE);
                    textInputLayoutPassword.setVisibility(View.VISIBLE);
                    textInputLayoutNama.setEnabled(true);
                    textInputLayoutEmail.setEnabled(true);
                    textInputLayoutPassword.setEnabled(true);
                    textInputLayoutNotelp.setEnabled(true);
                    textInputLayoutJenisKelamin.setEnabled(true);
                    textInputLayoutAlamat.setEnabled(true);
                    editProfilBtn.setText("Simpan Perubahan");
                } else {
                    updateProfilPegawai();
                    textInputLayoutNama.setVisibility(View.GONE);
                    textInputLayoutPassword.setVisibility(View.GONE);
                    textInputLayoutNama.setEnabled(false);
                    textInputLayoutEmail.setEnabled(false);
                    textInputLayoutPassword.setEnabled(false);
                    textInputLayoutNotelp.setEnabled(false);
                    textInputLayoutJenisKelamin.setEnabled(false);
                    textInputLayoutAlamat.setEnabled(false);
                    editProfilBtn.setText("Edit Profil");
                }
            }
        });

        // bottom bar

        homeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                startActivity(new Intent(ProfilActivity.this, HomeActivity.class));
            }
        });

        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ProfilActivity.this, TambahProdukActivity.class));
            }
        });

        autoCompleteTextViewJenisKelamin.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String item = adapterView.getItemAtPosition(i).toString();
                jenisKelamin = item;
            }
        });

    }

    private void getProfilPegawai() {
        int id = Integer.parseInt(sharedPreferences.getString("id", "0"));
        String token = sharedPreferences.getString("token", "token");
        String oldEmail = sharedPreferences.getString("email", "notfound");

        if (id!=0 && !token.equals("token")) {
            Call<Pegawai> call = apiCall.getPegawaiById(id, "Bearer " + token);

            call.enqueue(new Callback<Pegawai>() {
                @Override
                public void onResponse(Call<Pegawai> call, Response<Pegawai> response) {
                    if (response.isSuccessful()) {
                        textViewNama.setText(response.body().getNama());
                        textViewUsername.setText("@" + response.body().getUsername());
                        editTextNama.setText(response.body().getNama());
                        editTextNomorPegawai.setText(response.body().getNomorpegawai());
                        editTextEmail.setText(response.body().getEmail());
                        editTextPassword.setText("");
                        editTextNotelp.setText(response.body().getNotelp());
                        editTextAlamat.setText(response.body().getAlamat());

                        if(response.body().getJeniskelamin().equals("Laki-laki") || response.body().getJeniskelamin().equals("Perempuan")) {
                            autoCompleteTextViewJenisKelamin.setText(response.body().getJeniskelamin(), false);
                            jenisKelamin = response.body().getJeniskelamin();
                        }

                        if(jenisKelamin.equals("Laki-laki")) {
                            profilPict.setImageResource(R.drawable.profile_male);
                        } else {
                            profilPict.setImageResource(R.drawable.profile_female);
                        }
                    } else {
                        Toast.makeText(ProfilActivity.this, "Gagal mengambil data profil", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<Pegawai> call, Throwable t) {
                    Toast.makeText(ProfilActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(ProfilActivity.this, id + " " + token, Toast.LENGTH_SHORT).show();
        }
    }

    private void updateProfilPegawai() {
        int id = Integer.parseInt(sharedPreferences.getString("id", "0"));
        String token = sharedPreferences.getString("token", "token");
        String oldEmail = sharedPreferences.getString("email", "notfound");
        String newEmail = editTextEmail.getText().toString();
        String newPassword = editTextPassword.getText().toString();
        String username = textViewUsername.getText().toString().substring(1);
        String newNama = editTextNama.getText().toString();
        String newNotelp = editTextNotelp.getText().toString();
        String newAlamat = editTextAlamat.getText().toString();

        if(!oldEmail.equals("notfound")) {
            // jika user mengupdate password
            if (editTextPassword.length() > 0) {
                // update tabel pegawai
                Call<ResponseBody> updatePegawaiWithPassword = apiCall.updatePegawaiWithPassword(id, "Bearer " + token,
                        editTextNomorPegawai.getText().toString(),
                        newEmail,
                        oldEmail,
                        newPassword,
                        username,
                        newNama,
                        newNotelp,
                        newAlamat,
                        jenisKelamin);

                updatePegawaiWithPassword.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        int id = Integer.parseInt(sharedPreferences.getString("id", "0"));
                        String token = sharedPreferences.getString("token", "token");
                        String oldEmail = sharedPreferences.getString("email", "notfound");

                        if (response.isSuccessful()) {
                            // update tabel user
                            Call<ResponseBody> updateUserWithPassword = apiCall.updateUserWithPassword(id,"Bearer " + token,
                                    editTextEmail.getText().toString(),
                                    oldEmail,
                                    editTextPassword.getText().toString());

                            updateUserWithPassword.enqueue(new Callback<ResponseBody>() {
                                @Override
                                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                    if(response.isSuccessful()) {
                                        getProfilPegawai();
                                        Toast.makeText(ProfilActivity.this, "Sukses mengupdate data profil", Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toast.makeText(ProfilActivity.this, "Gagal mengupdate data profil", Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<ResponseBody> call, Throwable t) {
                                    Toast.makeText(ProfilActivity.this, "onFailure 1" + t.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            });
                        } else {
                            Toast.makeText(ProfilActivity.this, "Gagal mengupdate data profil", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Toast.makeText(ProfilActivity.this, "onFailure 2" + t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            } else {
                // update tabel pegawai (tanpa password)
                Call<ResponseBody> updatePegawaiWithoutPassword = apiCall.updatePegawaiWithoutPassword(id,"Bearer " + token,
                        editTextNomorPegawai.getText().toString(),
                        newEmail,
                        oldEmail,
                        username,
                        newNama,
                        newNotelp,
                        newAlamat,
                        jenisKelamin);

                updatePegawaiWithoutPassword.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        int id = Integer.parseInt(sharedPreferences.getString("id", "0"));
                        String token = sharedPreferences.getString("token", "token");
                        String oldEmail = sharedPreferences.getString("email", "notfound");
                        if (response.isSuccessful()) {
                            // update tabel user tanpa password
                            Call<ResponseBody> updateUserWithoutPassword = apiCall.updateUserWithoutPassword(id,"Bearer " + token,
                                    editTextEmail.getText().toString(),
                                    oldEmail);

                            updateUserWithoutPassword.enqueue(new Callback<ResponseBody>() {
                                @Override
                                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                    if(response.isSuccessful()) {
                                        getProfilPegawai();
                                        Toast.makeText(ProfilActivity.this, "Sukses mengupdate data profil", Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toast.makeText(ProfilActivity.this, "Gagal mengupdate data profil", Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<ResponseBody> call, Throwable t) {
                                    Toast.makeText(ProfilActivity.this, "onFailure 1" + t.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            });
                        } else {
                            Toast.makeText(ProfilActivity.this, "Gagal mengupdate data profil", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Toast.makeText(ProfilActivity.this, "onFailure 2" + t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        } else {
            Toast.makeText(ProfilActivity.this, oldEmail, Toast.LENGTH_SHORT).show();
        }
    }
}