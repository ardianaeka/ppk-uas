package com.example.uassupermart.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.uassupermart.Model.Produk;
import com.example.uassupermart.Model.ProdukResponse;
import com.example.uassupermart.Model.Pegawai;
import com.example.uassupermart.R;
import com.example.uassupermart.Service.APICall;
import com.example.uassupermart.Service.Retro;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity {

    ImageView logout;
    TextView lihatInventarisText, textViewNamaLengkap, banyakFood, banyakBeverage, banyakGrocery, banyakFreshveg, banyakDairy, banyakPersonal;
    LinearLayout inventarisBtn, tambahBarangBtn, pegawaiBtn, profilBtn, infoBtn, settingBtn, profileBtn, linearLayout2;
    FloatingActionButton addBtn;
    SharedPreferences sharedPreferences;
    long nKat1, nKat2, nKat3, nKat4, nKat5, nKat6;
    ArrayList<Produk> produkArrayList;
    final APICall apiCall = Retro.getAPICall();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        sharedPreferences = getSharedPreferences("JWTTOKEN", 0);
        initView();
        getProduk();
        initOnClikListener();
    }

    private void getProduk() {
        String token = sharedPreferences.getString("token", "token");

        Call<ProdukResponse> arrayListBarang = apiCall.getProduct("Bearer " + token);

        arrayListBarang.enqueue(new Callback<ProdukResponse>() {
            @Override
            public void onResponse(Call<ProdukResponse> call, Response<ProdukResponse> response) {
                if(response.isSuccessful()) {
                    ArrayList<Produk> products = response.body().getBarangArrayList();
                    produkArrayList = new ArrayList<>();
                    for  (int i = 0; i < products.size(); i++) {
                        produkArrayList.add(new Produk(products.get(i).getId(), products.get(i).getNama(), products.get(i).getHarga(), products.get(i).getKategori(), products.get(i).getStok()));
                    }

                    nKat1 = 0;
                    nKat2 = 0;
                    nKat3 = 0;
                    nKat4 = 0;
                    nKat5 = 0;
                    nKat6 = 0;

                    for (Produk b : produkArrayList) {
                        if (b.getKategori().equals("Food")) {
                            nKat1++;
                        }
                    }

                    for (Produk b : produkArrayList) {
                        if (b.getKategori().equals("Beverage")) {
                            nKat2++;
                        }
                    }

                    for (Produk b : produkArrayList) {
                        if (b.getKategori().equals("Grocery")) {
                            nKat3++;
                        }
                    }

                    for (Produk b : produkArrayList) {
                        if (b.getKategori().equals("Fresh & Vegetables")) {
                            nKat4++;
                        }
                    }

                    for (Produk b : produkArrayList) {
                        if (b.getKategori().equals("Dairy")) {
                            nKat5++;
                        }
                    }

                    for (Produk b : produkArrayList) {
                        if (b.getKategori().equals("Personal Care")) {
                            nKat6++;
                        }
                    }

                    banyakFood.setText(String.valueOf(nKat1));
                    banyakBeverage.setText(String.valueOf(nKat2));
                    banyakGrocery.setText(String.valueOf(nKat3));
                    banyakFreshveg.setText(String.valueOf(nKat4));
                    banyakDairy.setText(String.valueOf(nKat5));
                    banyakPersonal.setText(String.valueOf(nKat6));
                } else {
                    Toast.makeText(HomeActivity.this, "Token invalid", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ProdukResponse> call, Throwable t) {
                Toast.makeText(HomeActivity.this, "onFailure : " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initView() {
        logout = findViewById(R.id.logoutIcon);
        textViewNamaLengkap = findViewById(R.id.nama_lengkap);
        lihatInventarisText = findViewById(R.id.lihatInventarisText);
        // Menu
        linearLayout2 = findViewById(R.id.linearLayout2);
        inventarisBtn = findViewById(R.id.inventarisBtn);
        tambahBarangBtn = findViewById(R.id.tambahBarangBtn);
        pegawaiBtn = findViewById(R.id.pegawaiBtn);
        profilBtn = findViewById(R.id.profilBtn);
        infoBtn = findViewById(R.id.infoBtn);
        settingBtn = findViewById(R.id.settingBtn);
        // Bottom Bar
        addBtn = findViewById(R.id.addBtn);
        profileBtn = findViewById(R.id.profileBtn);
        // Statistik
        banyakFood = findViewById(R.id.banyakFood);
        banyakBeverage = findViewById(R.id.banyakBeverage);
        banyakGrocery = findViewById(R.id.banyakGrocery);
        banyakFreshveg = findViewById(R.id.banyakFreshveg);
        banyakDairy = findViewById(R.id.banyakDairy);
        banyakPersonal = findViewById(R.id.banyakPersonal);


        // jika role pengguna adalah bukan admin
        if (!sharedPreferences.getString("role", "user").equals("admin")) {
            // menyembunyikan menu pegawai dan add barang
            addBtn.setVisibility(View.GONE);
            linearLayout2.setVisibility(View.GONE);
        } else {
            addBtn.setVisibility(View.VISIBLE);
            linearLayout2.setVisibility(View.VISIBLE);
        }

        getProfilPegawai();
    }

    private void initOnClikListener() {
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);

                builder.setMessage("Apakah Anda yakin ingin logout?")
                        .setTitle("Konfirmasi");

                builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.clear();
                        editor.commit();
                        startActivity(new Intent(HomeActivity.this, LoginActivity.class));
                        finish();
                    }
                });

                builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        lihatInventarisText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, InventarisActivity.class));
            }
        });

        inventarisBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, InventarisActivity.class));
            }
        });

        tambahBarangBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, TambahProdukActivity.class));
            }
        });

        pegawaiBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, PegawaiActivity.class));
            }
        });

        profilBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, ProfilActivity.class));
            }
        });

        infoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, AboutActivity.class));
            }
        });

        settingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, SettingsActivity.class));
            }
        });

        // bottom bar

        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, TambahProdukActivity.class));
            }
        });

        profileBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, ProfilActivity.class));
            }
        });
    }

    private void getProfilPegawai() {
        int id = Integer.parseInt(sharedPreferences.getString("id", "0"));
        String token = sharedPreferences.getString("token", "token");
        String oldEmail = sharedPreferences.getString("email", "notfound");

        if (id!=0 && !token.equals("token")) {
            Call<Pegawai> call = apiCall.getPegawaiById(id, "Bearer " + token);

            call.enqueue(new Callback<Pegawai>() {
                @Override
                public void onResponse(Call<Pegawai> call, Response<Pegawai> response) {
                    if (response.isSuccessful()) {
                        textViewNamaLengkap.setText(response.body().getNama());
                    } else {
                        Toast.makeText(HomeActivity.this, "Gagal mengambil data profil", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<Pegawai> call, Throwable t) {
                    Toast.makeText(HomeActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(HomeActivity.this, "Token invalid", Toast.LENGTH_SHORT).show();
        }
    }
}