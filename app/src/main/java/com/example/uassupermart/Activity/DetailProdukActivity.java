package com.example.uassupermart.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.uassupermart.Model.Produk;
import com.example.uassupermart.R;
import com.example.uassupermart.Service.APICall;
import com.example.uassupermart.Service.Retro;
import com.google.android.material.textfield.TextInputLayout;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailProdukActivity extends AppCompatActivity {
    ImageView backBtn;
    EditText editTextNama, editTextHarga, editTextStok;
    TextInputLayout textInputNama, textInputHarga, textInputStok, textInputKategori;
    AutoCompleteTextView autoCompleteTextViewKategori;
    ArrayAdapter<String> adapterItems;
    String kategori;
    String[] listKategori = {"Food", "Beverage", "Grocery", "Fresh & Vegetables", "Dairy", "Personal Care"};
    Button editButton, deleteButton;

    Produk produk;
    SharedPreferences sharedPreferences;
    final APICall apiCall = Retro.getAPICall();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_produk);

        sharedPreferences = getSharedPreferences("JWTTOKEN", 0);

        initView();
        initOnClickListener();
        getBundle();
    }

    private void initView() {
        backBtn = findViewById(R.id.backBtn);
        editTextNama = findViewById(R.id.editTextNama);
        editTextHarga = findViewById(R.id.editTextHarga);
        editTextStok = findViewById(R.id.editTextStok);
        autoCompleteTextViewKategori = findViewById(R.id.autoCompleteTextKategori);
        editButton = findViewById(R.id.editButton);
        deleteButton = findViewById(R.id.deleteButton);
        textInputNama = findViewById(R.id.textInputNama);
        textInputHarga = findViewById(R.id.textInputHarga);
        textInputStok = findViewById(R.id.textInputStok);
        textInputKategori = findViewById(R.id.textInputKategori);

        adapterItems = new ArrayAdapter<String>(this, R.layout.list_kategori, listKategori);
        autoCompleteTextViewKategori.setAdapter(adapterItems);

        // jika role pengguna adalah bukan admin
        if (!sharedPreferences.getString("role", "user").equals("admin")) {
            // menyembunyikan button delete barang
            deleteButton.setVisibility(View.GONE);
        } else {
            deleteButton.setVisibility(View.VISIBLE);
        }
    }

    private void initOnClickListener() {
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DetailProdukActivity.this, InventarisActivity.class));
                finish();
            }
        });

        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(editButton.getText().toString().equals("Edit Produk")) {
                    if (sharedPreferences.getString("role", "user").equals("admin")) {
                        textInputNama.setEnabled(true);
                        textInputKategori.setEnabled(true);
                    }
                    textInputHarga.setEnabled(true);
                    textInputStok.setEnabled(true);
                    editButton.setText("Simpan Perubahan");
                } else {
                    updateProduk();
                    textInputNama.setEnabled(false);
                    textInputHarga.setEnabled(false);
                    textInputStok.setEnabled(false);
                    textInputKategori.setEnabled(false);
                    editButton.setText("Edit Produk");
                }
            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Create the alert dialog builder
                AlertDialog.Builder builder = new AlertDialog.Builder(DetailProdukActivity.this);

                // Set the message and title
                builder.setMessage("Apakah Anda yakin ingin menghapus produk ini?")
                        .setTitle("Konfirmasi");

                // Add the buttons
                builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User clicked OK button
                        // Perform the desired action here
                        deleteProduk();
                    }
                });

                builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                        // Do nothing
                    }
                });

                // Create and show the alert dialog
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        autoCompleteTextViewKategori.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String item = adapterView.getItemAtPosition(i).toString();
                kategori = item;
            }
        });
    }

    private void getBundle() {
        produk = (Produk) getIntent().getSerializableExtra("produk");
        String id = produk.getId();

        editTextNama.setText(produk.getNama());
        editTextHarga.setText(produk.getHarga());
        editTextStok.setText(produk.getStok());
        autoCompleteTextViewKategori.setText(produk.getKategori(), false);
        kategori = produk.getKategori();
    }


    private void updateProduk() {
        String token = sharedPreferences.getString("token", "token");
        if(!token.equals("token")) {
            Call<ResponseBody> updateProduct = apiCall.updateProduct(Integer.parseInt(produk.getId()), "Bearer " + token,
                    editTextNama.getText().toString(),
                    editTextHarga.getText().toString(),
                    editTextStok.getText().toString(),
                    kategori
            );

            updateProduct.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if(response.isSuccessful()) {
                        Toast.makeText(DetailProdukActivity.this, "Sukses mengupdate data produk", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(DetailProdukActivity.this, "Gagal mengupdate data produk" + response.code(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Toast.makeText(DetailProdukActivity.this, "onFailure 1" + t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(DetailProdukActivity.this, "Token Required", Toast.LENGTH_SHORT).show();
        }
    }

    private void deleteProduk() {
        String token = sharedPreferences.getString("token", "token");
        if (!token.equals("token")) {
            Call<ResponseBody> deleteProduct = apiCall.deleteProduct(Integer.parseInt(produk.getId()), "Bearer " + token);
            deleteProduct.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful()) {
                        Toast.makeText(DetailProdukActivity.this, "Sukses menghapus produk", Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Toast.makeText(DetailProdukActivity.this, "Gagal menghapus produk " + produk.getId() + " : " + response.code() + " " + response.message(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Toast.makeText(DetailProdukActivity.this, "onFailure " + t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(DetailProdukActivity.this, "Token Required", Toast.LENGTH_SHORT).show();
        }
    }
}