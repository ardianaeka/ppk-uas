package com.example.uassupermart.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.uassupermart.Model.Pegawai;
import com.example.uassupermart.ListAdapter.PegawaiListAdapter;
import com.example.uassupermart.Model.PegawaiResponse;
import com.example.uassupermart.R;
import com.example.uassupermart.RecyclerViewInterface;
import com.example.uassupermart.Service.APICall;
import com.example.uassupermart.Service.Retro;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PegawaiActivity extends AppCompatActivity implements RecyclerViewInterface {
    private RecyclerView recyclerViewList;
    private PegawaiListAdapter adapter;
    private ArrayList<Pegawai> pegawaiArrayList = new ArrayList<>();

    ImageView backBtn;
    SharedPreferences sharedPreferences;
    final APICall apiCall = Retro.getAPICall();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pegawai);

        sharedPreferences = getSharedPreferences("JWTTOKEN", 0);
        initView();
        initOnClickListener();
        getPegawai();

        recyclerViewList = (RecyclerView) findViewById(R.id.recyclerView);
        adapter = new PegawaiListAdapter(pegawaiArrayList, this);
        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(PegawaiActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerViewList.setLayoutManager(linearLayoutManager);
        recyclerViewList.setAdapter(adapter);
    }

    private void initView() {
        backBtn = findViewById(R.id.backBtn);
    }

    private void initOnClickListener() {
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    public void onItemClick(int position) {
        Intent i = new Intent(PegawaiActivity.this, DetailPegawaiActivity.class);
        Pegawai pegawai = pegawaiArrayList.get(position);
        Bundle bundle = new Bundle();
        bundle.putSerializable("pegawai", pegawai);
        i.putExtras(bundle);
        startActivity(i);
        finish();
    }

    private void getPegawai() {
        String token = sharedPreferences.getString("token", "token");

        Call<PegawaiResponse> arrayListPegawai = apiCall.getPegawai("Bearer " + token);

        arrayListPegawai.enqueue(new Callback<PegawaiResponse>() {
            @Override
            public void onResponse(Call<PegawaiResponse> call, Response<PegawaiResponse> response) {
                if(response.isSuccessful()) {
                    ArrayList<Pegawai> employees = response.body().getPegawaiArrayList();
                    for  (int i = 0; i < employees.size(); i++) {
                        pegawaiArrayList.add(new Pegawai(employees.get(i).getId(),
                                employees.get(i).getNomorpegawai(),
                                employees.get(i).getEmail(),
                                employees.get(i).getPassword(),
                                employees.get(i).getUsername(),
                                employees.get(i).getNama(),
                                employees.get(i).getNotelp(),
                                employees.get(i).getAlamat(),
                                employees.get(i).getJeniskelamin()
                        ));
                    }

                    adapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(PegawaiActivity.this, "Token invalid", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<PegawaiResponse> call, Throwable t) {
                Toast.makeText(PegawaiActivity.this, "onFailure : " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

}