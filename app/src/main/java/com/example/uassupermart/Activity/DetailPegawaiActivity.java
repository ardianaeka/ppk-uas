package com.example.uassupermart.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.app.AlertDialog;
import android.content.DialogInterface;


import com.example.uassupermart.Model.Pegawai;
import com.example.uassupermart.R;
import com.example.uassupermart.Service.APICall;
import com.example.uassupermart.Service.Retro;
import com.google.android.material.textfield.TextInputLayout;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailPegawaiActivity extends AppCompatActivity {

    TextView textViewNama, textViewUsername;
    TextInputLayout textInputLayoutNomorPegawai, textInputLayoutEmail, textInputLayoutNotelp, textInputLayoutJenisKelamin, textInputLayoutAlamat;
    EditText editTextNomorPegawai, editTextEmail, editTextNotelp, editTextJenisKelamin, editTextAlamat;
    Button editProfilBtn, deleteButton;
    Pegawai pegawai;
    ImageView profilPict, backBtn;
    SharedPreferences sharedPreferences;
    final APICall apiCall = Retro.getAPICall();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_pegawai);

        sharedPreferences = getSharedPreferences("JWTTOKEN", 0);
        initView();
        initOnClickListener();
        getBundle();
    }

    private void initView() {
        profilPict = findViewById(R.id.profil_pict);
        backBtn = findViewById(R.id.backBtn);
        // TextView
        textViewNama = findViewById(R.id.nama);
        textViewUsername = findViewById(R.id.username);
        // Text Input Layout
        textInputLayoutNomorPegawai = findViewById(R.id.textInputLayoutNomorPegawai);
        textInputLayoutEmail = findViewById(R.id.textInputLayoutEmail);
        textInputLayoutNotelp = findViewById(R.id.textInputLayoutNotelp);
        textInputLayoutJenisKelamin = findViewById(R.id.textInputLayoutJenisKelamin);
        textInputLayoutAlamat = findViewById(R.id.textInputLayoutAlamat);
        // Edit Text
        editTextNomorPegawai = findViewById(R.id.editTextNomorPegawai);
        editTextEmail = findViewById(R.id.editTextEmail);
        editTextNotelp = findViewById(R.id.editTextNotelp);
        editTextJenisKelamin = findViewById(R.id.editTextJenisKelamin);
        editTextAlamat = findViewById(R.id.editTextAlamat);
        // Button
        editProfilBtn = findViewById(R.id.editProfilBtn);
        deleteButton = findViewById(R.id.deleteButton);
    }

    private void initOnClickListener() {
        editProfilBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(editProfilBtn.getText().toString().equals("Edit Nomor Pegawai")) {
                    textInputLayoutNomorPegawai.setEnabled(true);
                    editProfilBtn.setText(R.string.simpan_perubahan);
                } else {
                    updateNomorPegawai();
                    textInputLayoutNomorPegawai.setEnabled(false);
                    editProfilBtn.setText(R.string.edit_nomor_pegawai);
                }
            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Create the alert dialog builder
                AlertDialog.Builder builder = new AlertDialog.Builder(DetailPegawaiActivity.this);

                // Set the message and title
                builder.setMessage("Apakah Anda yakin ingin menghapus data pegawai ini?")
                        .setTitle("Konfirmasi");

                // Add the buttons
                builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User clicked OK button
                        // Perform the desired action here
                        deletePegawai();
                    }
                });

                builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                        // Do nothing
                    }
                });

                // Create and show the alert dialog
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DetailPegawaiActivity.this, PegawaiActivity.class));
                finish();
            }
        });
    }

    private void getBundle() {
        pegawai = (Pegawai) getIntent().getSerializableExtra("pegawai");
        String id = pegawai.getId();
        textViewNama.setText(pegawai.getNama());
        textViewUsername.setText(String.format("@%s", pegawai.getUsername()));
        editTextNomorPegawai.setText(pegawai.getNomorpegawai());
        editTextEmail.setText(pegawai.getEmail());
        editTextNotelp.setText(pegawai.getNotelp());
        editTextJenisKelamin.setText(pegawai.getJeniskelamin());
        editTextAlamat.setText(pegawai.getAlamat());

        if(editTextJenisKelamin.getText().toString().equals("Laki-laki")) {
            profilPict.setImageResource(R.drawable.profile_male);
        } else {
            profilPict.setImageResource(R.drawable.profile_female);
        }
    }

    private void updateNomorPegawai() {
        String token = sharedPreferences.getString("token", "token");

        if (!token.equals("token")) {
            // update tabel pegawai
            Call<ResponseBody> updatePegawaiWithoutPassword = apiCall.updatePegawaiWithoutPassword(Integer.parseInt(pegawai.getId()), "Bearer " + token,
                    editTextNomorPegawai.getText().toString(),
                    editTextEmail.getText().toString(),
                    editTextEmail.getText().toString(),
                    textViewUsername.getText().toString().substring(1),
                    textViewNama.getText().toString(),
                    editTextNotelp.getText().toString(),
                    editTextAlamat.getText().toString(),
                    editTextJenisKelamin.getText().toString());

            updatePegawaiWithoutPassword.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful()) {
                        Toast.makeText(DetailPegawaiActivity.this, "Sukses mengupdate data nomor pegawai", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(DetailPegawaiActivity.this, "Gagal mengupdate data nomor pegawai", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Toast.makeText(DetailPegawaiActivity.this, "onFailure " + t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(DetailPegawaiActivity.this, token, Toast.LENGTH_SHORT).show();
        }
    }

    private void deletePegawai() {
        String token = sharedPreferences.getString("token", "token");

        Call<ResponseBody> deletePegawai = apiCall.deletePegawai(Integer.parseInt(pegawai.getId()), "Bearer " + token);

        deletePegawai.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful()) {
                    Call<ResponseBody> deleteUser = apiCall.deleteUser(Integer.parseInt(pegawai.getId()), "Bearer " + token);

                    deleteUser.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            if (response.isSuccessful()) {
                                Toast.makeText(DetailPegawaiActivity.this, "Sukses menghapus data pegawai", Toast.LENGTH_SHORT).show();
                                finish();
                            } else {
                                Toast.makeText(DetailPegawaiActivity.this, "Gagal menghapus data user", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            Toast.makeText(DetailPegawaiActivity.this, "onFailure " + t.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
                } else {
                    Toast.makeText(DetailPegawaiActivity.this, "Gagal menghapus data pegawai", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(DetailPegawaiActivity.this, "onFailure " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}