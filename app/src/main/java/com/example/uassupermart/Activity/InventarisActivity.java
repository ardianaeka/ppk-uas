package com.example.uassupermart.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.uassupermart.Model.Produk;
import com.example.uassupermart.ListAdapter.ProdukListAdapter;
import com.example.uassupermart.Model.ProdukResponse;
import com.example.uassupermart.R;
import com.example.uassupermart.RecyclerViewInterface;
import com.example.uassupermart.Service.APICall;
import com.example.uassupermart.Service.Retro;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InventarisActivity extends AppCompatActivity implements RecyclerViewInterface {
    private RecyclerView recyclerViewList;
    private ProdukListAdapter adapter;
    private ArrayList<Produk> produkArrayList = new ArrayList<>();
    private SearchView searchView;

    LinearLayout linearLayoutKategori;
    TextView notFoundText;
    ImageView backBtn, notFoundImage;
    FloatingActionButton addBtn;
    SharedPreferences sharedPreferences;
    final APICall apiCall = Retro.getAPICall();
    ArrayList<Produk> filteredProduct = produkArrayList;
    ArrayList<Produk> searchedProduct = filteredProduct;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventaris);

        sharedPreferences = getSharedPreferences("JWTTOKEN", 0);
        initView();
        getProduk();

        recyclerViewList = (RecyclerView) findViewById(R.id.recyclerView);
        adapter = new ProdukListAdapter(produkArrayList, this);
        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(InventarisActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerViewList.setLayoutManager(linearLayoutManager);
        recyclerViewList.setAdapter(adapter);
    }

    private void initView() {
        backBtn = findViewById(R.id.backBtn);
        addBtn = findViewById(R.id.addBtn);
        addBtn.requestFocus();
        searchView = findViewById(R.id.searchView);
        notFoundText = findViewById(R.id.notFoundText);
        notFoundImage = findViewById(R.id.notFoundImage);
        linearLayoutKategori = findViewById(R.id.linearLayoutKategori);

        for (int i = 0; i < linearLayoutKategori.getChildCount(); i++) {
            TextView child = (TextView) linearLayoutKategori.getChildAt(i);
            int finalI = i;
            child.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = linearLayoutKategori.indexOfChild(view);
                    TextView textView = (TextView) view;
                    String kategori = textView.getText().toString();

                    filteredProduct = new ArrayList<>();

                    if (kategori.equals("Semua")) {
                        filteredProduct = produkArrayList;
                    } else {
                        for (Produk b: produkArrayList) {
                            if(b.getKategori().equals(kategori)) {
                                filteredProduct.add(b);
                            }
                        }
                    }

                    adapter.setFilteredProduct(filteredProduct);
                    searchedProduct = filteredProduct;

                    if(filteredProduct.isEmpty()) {
                        notFoundText.setVisibility(View.VISIBLE);
                        notFoundImage.setVisibility(View.VISIBLE);
                    } else {
                        notFoundText.setVisibility(View.GONE);
                        notFoundImage.setVisibility(View.GONE);
                    }

                    for (int a = 0; a < linearLayoutKategori.getChildCount(); a++) {
                        if (a == position) {
                            textView.setBackground(ContextCompat.getDrawable(InventarisActivity.this, R.drawable.product_filter_selected));
                            textView.setTextColor(ContextCompat.getColor(InventarisActivity.this, R.color.white));
                        } else {
                            TextView child = (TextView) linearLayoutKategori.getChildAt(a);
                            child.setBackground(ContextCompat.getDrawable(InventarisActivity.this, R.drawable.product_filter));
                            child.setTextColor(ContextCompat.getColor(InventarisActivity.this, R.color.purple3));
                        }
                    }
                }
            });
        }

        searchView.clearFocus();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                filterList(newText);
                return false;
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(InventarisActivity.this, TambahProdukActivity.class));
            }
        });

        // jika role pengguna adalah bukan admin
        if (!sharedPreferences.getString("role", "user").equals("admin")) {
            // menyembunyikan button add barang
            addBtn.setVisibility(View.GONE);
        } else {
            addBtn.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onItemClick(int position) {
        Intent i = new Intent(InventarisActivity.this, DetailProdukActivity.class);
        Produk produk = searchedProduct.get(position);
        Bundle bundle = new Bundle();
        bundle.putSerializable("produk", produk);
        i.putExtras(bundle);
        startActivity(i);
        finish();
    }

    private void getProduk() {
        String token = sharedPreferences.getString("token", "token");

        Call<ProdukResponse> arrayListBarang = apiCall.getProduct("Bearer " + token);

        arrayListBarang.enqueue(new Callback<ProdukResponse>() {
            @Override
            public void onResponse(Call<ProdukResponse> call, Response<ProdukResponse> response) {
                if(response.isSuccessful()) {
                    ArrayList<Produk> products = response.body().getBarangArrayList();
                    for  (int i = 0; i < products.size(); i++) {
                        produkArrayList.add(new Produk(products.get(i).getId(), products.get(i).getNama(), products.get(i).getHarga(), products.get(i).getKategori(), products.get(i).getStok()));
                    }
                    adapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(InventarisActivity.this, "Token invalid", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ProdukResponse> call, Throwable t) {
                Toast.makeText(InventarisActivity.this, "onFailure : " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void filterList(String text) {
        searchedProduct = new ArrayList<>();
        for (Produk b: filteredProduct) {
            if(b.getNama().toLowerCase().contains(text.toLowerCase())) {
                searchedProduct.add(b);
            }
        }

        adapter.setFilteredProduct(searchedProduct);
        if(searchedProduct.isEmpty()) {
            notFoundText.setVisibility(View.VISIBLE);
            notFoundImage.setVisibility(View.VISIBLE);
        } else {
            notFoundText.setVisibility(View.GONE);
            notFoundImage.setVisibility(View.GONE);
        }
    }
}