package com.example.uassupermart.Service;

import android.content.Context;
import android.content.SharedPreferences;

public class TokenManager {
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private int Mode = 0;
    private static final String REFNAME = "JWTTOKEN";
    private static final String KEY_ID = "id";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_ROLE = "role";
    private static final String KEY_JWT_TOKEN = "token";
    private static final String IS_LOGGED_IN = "isLoggedIn";
    private Context context;

    public TokenManager(Context context) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences(REFNAME, Mode);
        editor = sharedPreferences.edit();
    }

    public void createSession(String id, String email, String role, String token, Boolean isLoggedIn) {
        editor.putString(KEY_ID, id);
        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_ROLE, role);
        editor.putString(KEY_JWT_TOKEN, token);
        editor.putBoolean(IS_LOGGED_IN, isLoggedIn);
        editor.commit();
    }
}
