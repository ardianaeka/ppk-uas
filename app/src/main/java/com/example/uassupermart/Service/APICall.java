package com.example.uassupermart.Service;

import com.example.uassupermart.Model.Produk;
import com.example.uassupermart.Model.ProdukResponse;
import com.example.uassupermart.Model.JWTToken;
import com.example.uassupermart.Model.Pegawai;
import com.example.uassupermart.Model.PegawaiResponse;
import com.example.uassupermart.Model.User;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface APICall {
    @FormUrlEncoded
    @POST("login")
    Call<JWTToken> login(@Field("email") String email,
                         @Field("password") String password);

    @GET("me")
    Call<User> getUser(@Header("Authorization") String authToken);

    @FormUrlEncoded
    @POST("register")
    Call<JWTToken> register(@Field("nama") String nama,
                            @Field("username") String username,
                            @Field("email") String email,
                            @Field("password") String password,
                            @Field("confpassword") String confpassword,
                            @Field("role") String role);

    // PRODUCT
    @GET("product")
    Call<ProdukResponse> getProduct(@Header("Authorization") String authToken);

    @GET("product/{id}")
    Call<Produk> getProductById(@Path("id") int productId,
                                @Header("Authorization") String authToken);

    @FormUrlEncoded
    @POST("product")
    Call<ResponseBody> createProduct(@Header("Authorization") String authToken,
                               @Field("nama_produk") String nama_produk,
                               @Field("harga_satuan") String harga_satuan,
                               @Field("stok") String stok,
                               @Field("kategori") String kategori);

    @FormUrlEncoded
    @PUT("product/{id}")
    Call<ResponseBody> updateProduct(@Path("id") int productId,
                               @Header("Authorization") String authToken,
                               @Field("nama_produk") String nama_produk,
                               @Field("harga_satuan") String harga_satuan,
                               @Field("stok") String stok,
                               @Field("kategori") String kategori);

    @DELETE("product/{id}")
    Call<ResponseBody> deleteProduct(@Path("id") int productId,
                               @Header("Authorization") String authToken);

    // USER
    @FormUrlEncoded
    @PUT("user/{id}")
    Call<ResponseBody> updateUserWithPassword(@Path("id") int userId,
                                        @Header("Authorization") String authToken,
                                        @Field("email") String email,
                                        @Field("email_lama") String oldEmail,
                                        @Field("password") String password);

    @FormUrlEncoded
    @PUT("user/{id}")
    Call<ResponseBody> updateUserWithoutPassword(@Path("id") int userId,
                                           @Header("Authorization") String authToken,
                                           @Field("email") String email,
                                           @Field("email_lama") String oldEmail);

    @DELETE("user/{id}")
    Call<ResponseBody> deleteUser(@Path("id") int userId,
                                     @Header("Authorization") String authToken);

    // PEGAWAI
    @GET("pegawai")
    Call<PegawaiResponse> getPegawai(@Header("Authorization") String authToken);

    @GET("pegawai/{id}")
    Call<Pegawai> getPegawaiById(@Path("id") int pegawaiId,
                                @Header("Authorization") String authToken);

    @FormUrlEncoded
    @POST("pegawai")
    Call<ResponseBody> createPegawai(@Header("Authorization") String authToken,
                                     @Field("nomorpegawai") String nomorpegawai,
                                     @Field("email") String email,
                                     @Field("password") String password,
                                     @Field("username") String username,
                                     @Field("nama") String nama,
                                     @Field("notelp") String notelp,
                                     @Field("alamat") String alamat,
                                     @Field("jeniskelamin") String jeniskelamin);

    @FormUrlEncoded
    @PUT("pegawai/{id}")
    Call<ResponseBody> updatePegawaiWithPassword(@Path("id") int pegawaiId,
                                           @Header("Authorization") String authToken,
                                           @Field("nomorpegawai") String nomorpegawai,
                                           @Field("email") String email,
                                           @Field("email_lama") String oldEmail,
                                           @Field("password") String password,
                                           @Field("username") String username,
                                           @Field("nama") String nama,
                                           @Field("notelp") String notelp,
                                           @Field("alamat") String alamat,
                                           @Field("jeniskelamin") String jeniskelamin);

    @FormUrlEncoded
    @PUT("pegawai/{id}")
    Call<ResponseBody> updatePegawaiWithoutPassword(@Path("id") int pegawaiId,
                                              @Header("Authorization") String authToken,
                                              @Field("nomorpegawai") String nomorpegawai,
                                              @Field("email") String email,
                                              @Field("email_lama") String oldEmail,
                                              @Field("username") String username,
                                              @Field("nama") String nama,
                                              @Field("notelp") String notelp,
                                              @Field("alamat") String alamat,
                                              @Field("jeniskelamin") String jeniskelamin);

    @DELETE("pegawai/{id}")
    Call<ResponseBody> deletePegawai(@Path("id") int pegawaiId,
                               @Header("Authorization") String authToken);


}
