package com.example.uassupermart.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Produk implements Serializable {
    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("nama_produk")
    @Expose
    private String nama_produk;

    @SerializedName("harga_satuan")
    @Expose
    private String harga_satuan;

    @SerializedName("kategori")
    @Expose
    private String kategori;

    @SerializedName("stok")
    @Expose
    private String stok;

    public Produk(String id, String nama, String harga, String kategori, String stok) {
        this.id = id;
        this.nama_produk = nama;
        this.harga_satuan = harga;
        this.kategori = kategori;
        this.stok = stok;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama_produk;
    }

    public void setNama(String nama) {
        this.nama_produk = nama;
    }

    public String getHarga() {
        return harga_satuan;
    }

    public void setHarga(String harga) {
        this.harga_satuan = harga;
    }

    public String getKategori() {
        return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    public String getStok() {
        return stok;
    }

    public void setStok(String stok) {
        this.stok = stok;
    }
}
