package com.example.uassupermart.Model;

public class FilterModel {
    private String kategoriProduk;

    public String getKategoriProduk() {
        return kategoriProduk;
    }

    public void setKategoriProduk(String kategoriProduk) {
        this.kategoriProduk = kategoriProduk;
    }
}
