package com.example.uassupermart.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Pegawai implements Serializable {
    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("nomorpegawai")
    @Expose
    private String nomorpegawai;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("password")
    @Expose
    private String password;

    @SerializedName("username")
    @Expose
    private String username;

    @SerializedName("nama")
    @Expose
    private String nama;

    @SerializedName("notelp")
    @Expose
    private String notelp;

    @SerializedName("alamat")
    @Expose
    private String alamat;

    @SerializedName("jeniskelamin")
    @Expose
    private String jeniskelamin;

    public Pegawai(String id, String nomorpegawai, String email, String password, String username, String nama, String notelp, String alamat, String jeniskelamin) {
        this.id = id;
        this.nomorpegawai = nomorpegawai;
        this.email = email;
        this.password = password;
        this.username = username;
        this.nama = nama;
        this.notelp = notelp;
        this.alamat = alamat;
        this.jeniskelamin = jeniskelamin;
    }

    public String getId() { return id; }

    public void setId(String id) {
        this.id = id;
    }

    public String getNomorpegawai() {
        return nomorpegawai;
    }

    public void setNomorpegawai(String nomorpegawai) {
        this.nomorpegawai = nomorpegawai;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNotelp() {
        return notelp;
    }

    public void setNotelp(String notelp) {
        this.notelp = notelp;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getJeniskelamin() {
        return jeniskelamin;
    }

    public void setJeniskelamin(String jeniskelamin) {
        this.jeniskelamin = jeniskelamin;
    }
}
