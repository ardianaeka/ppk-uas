package com.example.uassupermart.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ProdukResponse {
    @SerializedName("produk")
    @Expose
    private ArrayList<Produk> produkArrayList;
    // other fields here

    public ArrayList<Produk> getBarangArrayList() {
        return produkArrayList;
    }
}
