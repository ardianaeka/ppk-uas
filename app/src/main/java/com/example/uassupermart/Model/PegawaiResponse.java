package com.example.uassupermart.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class PegawaiResponse {
    @SerializedName("pegawai")
    @Expose
    private ArrayList<Pegawai> pegawaiArrayList;
    // other fields here

    public ArrayList<Pegawai> getPegawaiArrayList() {
        return pegawaiArrayList;
    }
}
