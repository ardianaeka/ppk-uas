# Ujian Akhir Semester Pemrograman Platform Khusus 2022/2023

Nama : Ardiana Eka Prasiska <br/>
Kelas : 3SI3 <br/>
NIM : 222011813 <br/>

## Tentang Aplikasi

### SuperMart

SuperMart merupakan aplikasi inventarisasi berbasis android untuk sebuah supermarket bernama “SuperMart”. Pembangunan aplikasi bertujuan untuk memudahkan pegawai inventaris SuperMart dalam melakukan updating stok produk yang dijual. Fitur utama aplikasi yaitu pengelolaan inventaris dan pegawai. <br/>

### Tipe Pengguna

Terdapat dua tipe pengguna aplikasi, yakni admin dan user. Admin merupakan seorang kepala bagian inventaris atau manajer inventaris yang bertugas mengelola database produk dan pegawai mulai dari menambah, meng-update, serta menghapusnya. Sementara itu, user merupakan seorang pegawai bagian inventaris yang bertanggung jawab kepada manajer inventaris dan bertugas untuk meng-update stok produk pada supermarket. <br/>

### Client

Aplikasi client dibangun berbasis android menggunakan bahasa pemrograman Java. Adapun library tambahan yang digunakan yakni Retrofit2 dan OkHttp3 yang digunakan sebagai penghubung ke layanan web servcice untuk mengonsumsi API yang disediakan. <br/>

### Web Service

Aplikasi web serivce mengatasi pengelolaan database dan penyediaan API untuk setiap tabel dan dibangun dengan menggunakan framework CodeIgniter 4. Autentikasi dan autorisasi proses login dan pemberian hak akses telah berbasis token dengan menerapkan Java Web Token (JWT). Setiap pengguna akan memiliki token setelah berhasil melakukan login. Pemberian hak akses pengguna didasarkan pada token tersebut. <br/>

## Demonstrasi

Untuk melihat demonstrasi aplikasi, dapat diakses pada link berikut:

https://youtu.be/LyDKgvLI2Xs

## Source Code

Untuk source code aplikasi client (android) dapat diakses pada branch "main", sedangkan untuk source code aplikasi web service (API) dapat diakses pada branch "web-service"

<br/> Terima kasih :)
